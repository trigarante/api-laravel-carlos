import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Empleado} from '../interfaces/empleados.model';
import {Asignacion} from '../interfaces/asignacion.model';
import {Grupo} from '../interfaces/grupo.model';
import {Usuario} from '../interfaces/usuario.model';


@Injectable({
  providedIn: 'root'
})
export class AsignacionesService {
  private readonly baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = 'http://localhost:8000/';

  }


  getEmpleadosList(): Observable<Empleado[]> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.get<Empleado[]>(this.baseURL + 'empleados');
  }

  getCorreos(): Observable<Asignacion[]> {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.get<Asignacion[]>(this.baseURL + 'correos');
  }

  getGrupos(): Observable<Grupo[]> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.get<Grupo[]>(this.baseURL + 'grupos');
  }


  createUser(usuario: Usuario) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.baseURL + 'usuario', usuario);
  }


}
