import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Asignacion} from '../../interfaces/asignacion.model';
import {AsignacionesService} from '../../services/asignaciones.service';
import {Grupo} from '../../interfaces/grupo.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-modal-index',
  templateUrl: './modal-index.component.html',
  styleUrls: ['./modal-index.component.css']
})
export class ModalIndexComponent implements OnInit {
  correos: Asignacion[];
  grupos: Grupo[];

  constructor(public activeModal: NgbActiveModal, private asignacionesService: AsignacionesService, private formBuilder: FormBuilder, private router: Router) {
  }

  addForm: FormGroup;

  ngOnInit() {
    this.getCorreos();
    this.getGrupos();
    this.addForm = this.formBuilder.group({
      correo: ['', Validators.required],
      grupo: ['', Validators.required]
    });
  }

  getCorreos() {
    this.asignacionesService.getCorreos().subscribe(data => this.correos = data);
  }

  getGrupos() {
    this.asignacionesService.getGrupos().subscribe(data => this.grupos = data);
  }

  guardarUsuario() {
    this.asignacionesService.createUser(this.addForm.value)
      .subscribe(data => {
        this.router.navigate(['']);
      });
  }
}
