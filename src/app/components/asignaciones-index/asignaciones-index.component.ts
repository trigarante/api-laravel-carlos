import {Component, OnInit} from '@angular/core';
import {AsignacionesService} from '../../services/asignaciones.service';

import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {filter, map} from 'rxjs/operators';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Empleado} from '../../interfaces/empleados.model';
import {ModalIndexComponent} from '../modal-index/modal-index.component';


@Component({
  selector: 'app-asignaciones-index',
  templateUrl: './asignaciones-index.component.html',
  styleUrls: ['./asignaciones-index.component.css']
})
export class AsignacionesIndexComponent implements OnInit {
  empleados: Empleado[];
  cols: any[];


  constructor(private asignacionesService: AsignacionesService, private bsModalService: BsModalService, private modalService: NgbModal) {
  }

  getEmpleados() {
    this.asignacionesService.getEmpleadosList().subscribe(data => this.empleados = data);
  }

  openModal() {
    this.modalService.open(ModalIndexComponent);
  }

  ngOnInit() {
    this.cols = [
      {field: 'id', header: 'Id'},
      {field: 'nombre', header: 'Nombre'},
      {field: 'apellido_paterno' + 'apellido_materno', header: 'Apellido Paterno'},
    ];
    this.getEmpleados();
  }

}
