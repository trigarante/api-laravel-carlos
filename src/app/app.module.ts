import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ModalModule, BsModalService} from 'ngx-bootstrap/modal';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AsignacionesIndexComponent} from './components/asignaciones-index/asignaciones-index.component';
import {AsignacionesService} from './services/asignaciones.service';
import {ModalIndexComponent} from './components/modal-index/modal-index.component';
import {TableModule} from 'primeng/table';



@NgModule({
  declarations: [
    AppComponent,
    AsignacionesIndexComponent,
    ModalIndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ModalModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    TableModule
  ],
  providers: [AsignacionesService, BsModalService],
  bootstrap: [AppComponent],
  entryComponents: [ModalIndexComponent]
})
export class AppModule {
}
