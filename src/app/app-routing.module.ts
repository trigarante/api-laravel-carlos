import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AsignacionesIndexComponent} from './components/asignaciones-index/asignaciones-index.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'asignaciones',
    pathMatch: 'full'
  },
  {path: 'asignaciones', component: AsignacionesIndexComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
